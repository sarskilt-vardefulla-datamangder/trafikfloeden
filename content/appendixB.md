# Exempel i JSON

Samma entitet som i Appendix A, fast nu som JSON.

```json
{
  "addressCountry": "Sweden",
  "addressLocality": "Stockholm",
  "addressRegion": "Stockholm County",
  "district": "Östermalm",
  "postalCode": "114 52",
  "streetAddress": "Strandvägen 1",
  "averageGapDistance": 2.5,
  "averageHeadwayTime": "00:01:30",
  "averageVehicleLength": 4.8,
  "averageVehicleSpeed": 60,
  "congested": false,
  "dateCreated": "2024-05-21T10:30:00Z",
  "dateModified": "2024-05-21T10:35:00Z",
  "dateObserved": "2024-05-21T10:00:00Z",
  "description": "Traffic flow observation at Strandvägen",
  "id": "TF-001",
  "intensity": 120,
  "laneDirection": "forward",
  "laneId": 3,
  "location": {
    "type": "Point",
    "coordinates": [18.085086, 59.333482]
  },
  "name": "Strandvägen Traffic Flow",
  "occupancy": "00:00:45",
  "refRoadSegment": "https://example.com/road_segment",
  "reversedLane": false,
  "type": "TrafficFlowObserved",
  "vehicleSubType": "Car",
  "vehicleType": "Car"
}
```