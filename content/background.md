**Bakgrund**

Syftet med denna specifikation är att beskriva information om trafikflöden på ett enhetligt och standardiserat vis. Arbetet baseras på datamodellen [TrafficFlowObserved](https://github.com/smart-data-models/dataModel.Transportation/blob/master/TrafficFlowObserved/doc/spec.md) från initiativet [Smart Data Models](https://smartdatamodels.org/). Skillnaden mellan dessa modeller är marginell då behovet att använda styrda vokabulär från svenska myndigheter och ett striktare schema inte bidrar till en annorlunda modell. Därav bör denna specifikation ses som en extension till den angivna modellen ovan. 

Specifikationen syftar till att ge kommuner i Sverige möjlighet att enkelt kunna sätta samman och publicera datamängd(er) som beskriver trafikflöden. Den syftar även till att göra det enklare för internationella användare som är tagare av datamängden.

Följande har deltagit:

**[Dataverkstad](https://www.vgregion.se/ov/dataverkstad/)** - Modellering och rådgivning.<br>
