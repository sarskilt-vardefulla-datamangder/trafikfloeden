# Datamodell

Datamodellen är tabulär, det innebär att varje rad motsvarar exakt en entitet och varje kolumn motsvarar en egenskap för den beskrivna entiteten. XX attribut är definierade, där de första Y är obligatoriska. Speciellt viktigt är att man anger de obligatoriska fält i datamodellen som anges. 


<div class="note" title="1">
Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och på engelska.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige.


</div>

<div class="ms_datatable">

| Namn                 | Kardinalitet | Datatyp | Beskrivning |
|----------------------|--------------|---------|-------------|
| addressCountry       |0..1              | text  | Landet. Till exempel Spanien. |
| addressLocality      |0..1              | text  | Lokaliteten där gatuadressen finns, och som ligger i regionen.  |
| addressRegion        |0..1              | text  | Regionen där lokaliteten finns, och som ligger i landet. |
| district             |0..1              | text  | Ett distrikt är en typ av administrativ indelning som i vissa länder förvaltas av den lokala regeringen |
| postalCode           |0..1              | text  | Postnumret. Till exempel 24004.  |
| streetAddress        |0..1              | text  | Gatuadressen.  |
| averageGapDistance   |0..1              | decimal  | Genomsnittligt avstånd mellan på varandra följande fordon.  |
| averageHeadwayTime   |0..1              | time | Genomsnittlig huvudvägstid. Huvudvägstid är den tid som förflutit mellan två på varandra följande fordon.  |
| averageVehicleLength |0..1              | decimal | Genomsnittlig längd på fordonen som passerar under observationsperioden.  |
| averageVehicleSpeed  |0..1              | decimal  | Genomsnittlig hastighet på fordonen som passerar under observationsperioden. |
| congested            |0..1              | boolean | Flaggar om det var trafikstockning under observationsperioden i den aktuella körfältet. Avsaknaden av detta attribut betyder ingen trafikstockning.  |
| dateCreated          |0..1              | dateTime | Tidsstämpel för skapande av enhet. Detta tilldelas vanligtvis av lagringsplattformen |
| dateModified         |0..1              | dateTime | Tidsstämpel för senaste ändring av enhet. Detta tilldelas vanligtvis av lagringsplattformen |
| dateObserved         |0..1              | text  | Datum och tid för denna observation i ISO8601 UTC-format. Det kan representeras av en specifik tidpunkt eller av ett ISO8601-intervall. Som en tillfällig lösning för bristen på stöd av Orion Context Broker för tidsintervall kan två separata attribut användas: dateObservedFrom, dateObservedTo. Datum och tid eller ett ISO8601-intervall representerat som text.|
| dateObservedFrom     |0..1              | dateTime | Observationsperiodens startdatum och tid. Se dateObserved.  |
| dateObservedTo       |0..1              | dateTime | Observationsperiodens slutdatum och tid. Se dateObserved. |
| description          |0..1              | text  | En beskrivning av detta objekt |
| id                   |1              | text      | Unikt identifierare för enheten |
| intensity            |0..1              | heltal  | Totalt antal fordon som upptäckts under denna observationsperiod. |
| laneDirection        |0..1              | text  | Vanlig färdriktning i körfältet som hänvisas till i denna observation. Detta attribut är användbart när observationen inte hänvisar till någon vägsegment, vilket gör det möjligt att känna till färdriktningen för den observerade trafikflödet. Enum: forward, backward. Se RoadSegment för en beskrivning av dessa värders semantik. |
| laneId               |0..1              | integer  | Körfältsidentifierare. Körfältsidentifiering görs enligt konventionerna definierade av RoadSegment-enheten som är baserade på OpenStreetMap. |
| location             | 1             | JSON       | Geojson-referens till objektet. Det kan vara Point, LineString, Polygon, MultiPoint, MultiLineString eller MultiPolygon |
| name                 |0..1              | text | Namnet på detta objekt |
| occupancy            |0..1              | time  | Andel av observationstiden där ett fordon har ockuperat det observerade körfältet.  |
| refRoadSegment       |0..1              | uri     | Berört vägsegment där observationen har gjorts. Referens till en enhet av typ RoadSegment. |
| reversedLane         |0..1              | boolean | Flaggar om trafik i körfältet var omvänd under observationsperioden. Avsaknaden av detta attribut betyder ingen körfältsomvändning. |
| type                 |0..1              | text  | NGSI Entity-typ. Det måste vara TrafficFlowObserved |
| vehicleSubType       |0..1              | begrepp  | Det tillåter att specificera en undertyp av vehicleType, t.ex. om vehicleType är satt till Lorry kan vehicleSubType vara OGV1 eller OGV2 för att förmedla mer information om den exakta typen av fordon |
| vehicleType          |0..1              | begrepp | Fordonstyp ur strukturella egenskaper. Hämtas ur terminologin från Transportsstyrelsen.|



</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, dvs inga inledande eller eftersläpande tecken tillåts.

### **heltal**
Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en radda siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.
 
### **decimal**
Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas då den tabulära modellen använder komma som separator.

Den kanoniska representationen i xsd:decimal är påbjuden, d.v.s. inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **url**

En länk till en webbsida där entiteten presenteras hos den lokala myndigheten eller organisationen.

Observera att man inte får utelämna schemat, d.v.s. "www.example.com" är inte en tillåten webbadress, däremot är "https://www.example.com" ok. Relativa webbadresser accepteras inte heller. (Ett fullständigt reguljärt uttryck utelämnas då den är både för omfattande och opedagogisk.)

Om du behöver ange flera URL:er måste du då sätta dubbelt citattecken och separera de olika URL:erna med kommatecken. Läs gärna med i RFC 4180 för CSV.

Kontrollera även innan du publicerar filen att det går att läsa in din fil utan problem. Det finns flera webbverktyg för att testa så kallad [parsing](https://sv.wikipedia.org/wiki/Parser) t.ex. [https://CSVLint.io](https://csvlint.io) som du kan använda för att testa din fil.

### **boolean**

Reguljärt uttryck: **`/^[true|false]?&/`**

I samtliga förekommande fall kan texten “true” eller “false” utelämnas.

Ett tomt fält skall tolkas som “okänt” eller “ej inventerat”. Ange enbart “true” eller “false” om du vet att egenskapen finns (true) eller saknas (false). Gissa aldrig.

Attributet är en så kallad Boolesk” datatyp och kan antingen ha ett av två värden: TRUE eller FALSE, men aldrig båda.

### **dateTime** 

Reguljärt uttryck: **`\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d(?:\.\d+)?Z?`**

dateTime värden kan ses som objekt med heltalsvärdena år, månad, dag, timme och minut, en decimalvärderad sekundegenskap och en tidszonegenskap.

[XML schema](https://www.w3.org/TR/xmlschema-2/#dateTime) 
</br>

[IS0 8601](https://en.wikipedia.org/wiki/ISO_8601)

Värden som kan anges för att beskriva ett datum:

YYYY-MM-DD
</br>
YYYY-MM
</br>
YYYYMMDD 
</br>
</div>

Observera att ni inte kan ange: **YYYYMM**    

För att beskriva tider uttrycker ni det enligt nedan:

**T** representerar tid, **hh** avser en nollställd timme mellan 00 och 24, **mm** avser en nollställd minut mellan 00 och 59 och **ss** avser en nollställd sekund mellan 00 och 60.

Thh:mm:ss.sss eller	Thhmmss.sss 
</br>
Thh:mm:ss eller Thhmmss
</br>
Thh:mm eller Thhmm
</br>

</div>

Exempel - XX som äger rum kl 17:00 den 22 februari 2023 

</br>
2024-02-22T17:00

</br>
</br>

20240222T1700

</div>

## Förtydligande av attribut

### addressCountry
Reguljärt uttryck: **`/^[a-zA-Z\s]*$/`**

Landet. Till exempel Spanien. Modell: [https://schema.org/addressCountry](https://schema.org/addressCountry)

### addressLocality
Reguljärt uttryck: **`/^[a-zA-Z\s]*$/`**

Lokaliteten där gatuadressen finns, och som ligger i regionen. Modell: [https://schema.org/addressLocality](https://schema.org/addressLocality)

### addressRegion
Reguljärt uttryck: **`/^[a-zA-Z\s]*$/`**

Regionen där lokaliteten finns, och som ligger i landet. Modell: [https://schema.org/addressRegion](https://schema.org/addressRegion)

### district
Reguljärt uttryck: **`/^[a-zA-Z\s]*$/`**

Ett distrikt är en typ av administrativ indelning som i vissa länder förvaltas av den lokala regeringen

### postalCode
Reguljärt uttryck: **`/^\d+$/`**

Postnumret. Till exempel 24004. Modell: [https://schema.org/https://schema.org/postalCode](https://schema.org/https://schema.org/postalCode)

### streetAddress
Reguljärt uttryck: **`/^[\w\s.,-]*$/`** 

Gatuadressen. Modell: [https://schema.org/streetAddress](https://schema.org/streetAddress)


### averageGapDistance
Reguljärt uttryck: **`/^\d+(\.\d+)?$/`**

Genomsnittligt avstånd mellan på varandra följande fordon. 

### averageHeadwayTime
Reguljärt uttryck: **`/^\d+(\.\d+)?$/`**

Genomsnittlig huvudvägstid. Huvudvägstid är den tid som förflutit mellan två på varandra följande fordon. 

### averageVehicleLength
Reguljärt uttryck: **`/^\d+(\.\d+)?$/`**

Genomsnittlig längd på fordonen som passerar under observationsperioden. 

### averageVehicleSpeed
Reguljärt uttryck: **`/^\d+(\.\d+)?$/`**

Genomsnittlig hastighet på fordonen som passerar under observationsperioden. 

### congested
Reguljärt uttryck: **`/^(true|false)$/`**

Flaggar om det var trafikstockning under observationsperioden i den aktuella körfältet. Avsaknaden av detta attribut betyder ingen trafikstockning. Modell: [https://schema.org/Boolean](https://schema.org/Boolean)


### dateCreated
Reguljärt uttryck: **`/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z$/`**  

Tidsstämpel för skapande av enhet. Detta tilldelas vanligtvis av lagringsplattformen

### dateModified
Reguljärt uttryck: **`/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z$/`**  

Tidsstämpel för senaste ändring av enhet. Detta tilldelas vanligtvis av lagringsplattformen

### dateObserved
Reguljärt uttryck: **`/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z$/`**  

Datum och tid för denna observation i ISO8601 UTC-format. Det kan representeras av en specifik tidpunkt eller av ett ISO8601-intervall. Som en tillfällig lösning för bristen på stöd av Orion Context Broker för tidsintervall kan två separata attribut användas: dateObservedFrom, dateObservedTo. Datum och tid eller ett ISO8601-intervall representerat som text. Modell: [https://schema.org/DateTime](https://schema.org/DateTime)

### dateObservedFrom
Reguljärt uttryck: **`/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z$/`**  

Observationsperiodens startdatum och tid. Se dateObserved. Modell: [https://schema.org/Datetime](https://schema.org/Datetime)

### dateObservedTo
Reguljärt uttryck: **`/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z$/`**  

Observationsperiodens slutdatum och tid. Se dateObserved. Modell: [https://schema.org/Datetime](https://schema.org/Datetime)

### description
Reguljärt uttryck: **`/^[\w\s.,-]*$/`** 

En beskrivning av detta objekt

### id
Reguljärt uttryck: **`/^[\w-]*$/`**

Unikt identifierare för enheten

### intensity
Reguljärt uttryck: **`/^\d+(\.\d+)?$/`**

Totalt antal fordon som upptäckts under denna observationsperiod. 

### laneDirection
Reguljärt uttryck: **`/^(forward|backward)$/`**

Vanlig färdriktning i körfältet som hänvisas till i denna observation. Detta attribut är användbart när observationen inte hänvisar till någon vägsegment, vilket gör det möjligt att känna till färdriktningen för den observerade trafikflödet. Enum: forward, backward. Se RoadSegment för en beskrivning av dessa värders semantik. Modell: [https://schema.org/Text](https://schema.org/Text)

### laneId
Reguljärt uttryck: **`/^[a-zA-Z_:1-9]*$/`**

Körfältsidentifierare. Körfältsidentifiering görs enligt konventionerna definierade av RoadSegment-enheten som är baserade på OpenStreetMap. 

### location
Reguljärt uttryck: **`/^\{.*\}$/`**

Geojson-referens till objektet. Det kan vara Point, LineString, Polygon, MultiPoint, MultiLineString eller MultiPolygon

### name
Reguljärt uttryck: **`/^[\w\s]*$/`**

Namnet på detta objekt

### occupancy
Reguljärt uttryck: **`/^\d+(\.\d+)?$/`** 

Andel av observationstiden där ett fordon har ockuperat det observerade körfältet. 


### refRoadSegment
Reguljärt uttryck: **`/^https?:\/\/.*$/`**

Berört vägsegment där observationen har gjorts. Referens till en enhet av typ RoadSegment. Modell: [https://schema.org/URL](https://schema.org/URL)

### reversedLane
Reguljärt uttryck: **`/^(true|false)$/`** 

Flaggar om trafik i körfältet var omvänd under observationsperioden. Avsaknaden av detta attribut betyder ingen körfältsomvändning. Modell: [https://schema.org/Boolean](https://schema.org/Boolean)


### type
Reguljärt uttryck: **`/^[a-zA-Z]*$/`**

NGSI Entity-typ. Det måste vara TrafficFlowObserved

### vehicleSubType
Reguljärt uttryck: **`/^[\w\s]*$/`**

Tillåter att specificera en undertyp av vehicleType, till exempel om vehicleType är satt till lastbil kan vehicleSubType vara OGV1 eller OGV2 för att förmedla mer information om den exakta typen av fordon

### vehicleType
Reguljärt uttryck: **`/^[a-zA-Z\s]*$/`**

Typ av fordon ur dess strukturella egenskaper synvinkel. Enum: agriculturalVehicle, bicycle, bus, minibus, car, caravan, tram, tanker, carWithCaravan, carWithTrailer



 




