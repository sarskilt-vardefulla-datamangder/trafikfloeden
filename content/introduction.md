Denna specifikation anger vilka fält som är obligatoriska och rekommenderade. T.ex. gäller att man måste ange id. 29 attribut kan anges varav 2 är obligatoriska. 

I appendix A finns ett exempel på hur trafikflöden uttrycks i CSV. I appendix B uttrycks samma exempel i JSON. 

Denna specifikation definierar en enkel informationsmodell för trafikflöden. Specifikationen innefattar också en beskrivning av hur informationen uttrycks i formaten CSV och JSON. Som grund förutsätts CSV kunna levereras då det är ett praktiskt format och skapar förutsägbarhet för mottagare av informationen. Till denna specifikation finns det ett CSV-schema och ett JSON schema. 