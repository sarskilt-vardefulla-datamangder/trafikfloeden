## Exempel i CSV, kommaseparerad

<div class="example csvtext">
addressCountry,addressLocality,addressRegion,district,postalCode,streetAddress,averageGapDistance,averageHeadwayTime,averageVehicleLength,averageVehicleSpeed,congested,dateCreated,dateModified,dateObserved,description,id,intensity,laneDirection,laneId,location,name,occupancy,refRoadSegment,reversedLane,type,vehicleSubType,vehicleType
<br>
Sweden,Stockholm,Stockholm County,Östermalm,114 52,"Strandvägen 1",2.5,00:01:30,4.8,60,false,2024-05-21T10:30:00Z,2024-05-21T10:35:00Z,2024-05-21T10:00:00Z,"Traffic flow observation at Strandvägen",TF-001,120,forward,3,"{\"type\":\"Point\",\"coordinates\":[18.085086,59.333482]}",Strandvägen Traffic Flow,00:00:45,https://example.com/road_segment, false,TrafficFlowObserved,Car,Car
</div>
